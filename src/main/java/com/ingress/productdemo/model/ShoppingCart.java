package com.ingress.productdemo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ShoppingCarts")
@FieldDefaults(level = AccessLevel.PUBLIC)
@Builder
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String name;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinTable(
            name = "ShoppingCartProduct",
            joinColumns = @JoinColumn(name = "ShoppingCartId"),
            inverseJoinColumns = @JoinColumn(name = "ProductId")
    )
    private Set<Product> products = new HashSet<>();
}
