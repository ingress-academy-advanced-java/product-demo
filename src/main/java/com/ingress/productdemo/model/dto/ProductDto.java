package com.ingress.productdemo.model.dto;

import com.ingress.productdemo.model.CategoryEntity;
import com.ingress.productdemo.model.ProductDetail;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class ProductDto {

    Integer id;
    String name;
    Integer price;
    CategoryEntity category;
    ProductDetail productDetail;

}
