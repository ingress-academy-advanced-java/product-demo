package com.ingress.productdemo.repository;

import com.beyt.jdq.repository.JpaDynamicQueryRepository;
import com.ingress.productdemo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface ProductRepository extends JpaDynamicQueryRepository<Product, Integer> {

    Product findByName(String name);

}
