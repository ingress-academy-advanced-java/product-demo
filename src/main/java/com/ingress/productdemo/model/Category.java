package com.ingress.productdemo.model;

public enum Category {
    BOOK(1, "Kitab"),
    ELECTRONIC(2, "Elektronika"),
    HOUSEHOLD(3, "Məişət");

    private int id;
    private String name;

    Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Category getById(int id) {
        for (Category category : Category.values()) {
            if (category.getId() == id) {
                return category;
            }
        }
        return null;
    }

    public static Category getByName(String name) {
        for (Category category : Category.values()) {
            if (category.getName().equals(name)) {
                return category;
            }
        }
        return null;
    }
}
