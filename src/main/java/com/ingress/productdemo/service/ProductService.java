package com.ingress.productdemo.service;

import com.beyt.jdq.dto.Criteria;
import com.beyt.jdq.dto.enums.CriteriaOperator;
import com.beyt.jdq.dto.enums.Order;
import com.ingress.productdemo.helper.ProductMapper;
import com.ingress.productdemo.model.CategoryEntity;
import com.ingress.productdemo.model.Product;
import com.ingress.productdemo.model.dto.ProductDto;
import com.ingress.productdemo.repository.CategoryRepository;
import com.ingress.productdemo.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.beyt.jdq.query.builder.QuerySimplifier.*;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public ProductService(ProductRepository productRepository, com.ingress.productdemo.repository.CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<ProductDto> findAll() {
        return productRepository.findAll().stream().map(ProductMapper::toProductDto).toList();
    }

    public Page<ProductDto> findAllWithDynamicQuery(Integer priceFrom, Integer priceTo) {

        var criteriaList = new ArrayList<>();
        if (priceFrom != null) {
            criteriaList.add(
                    Criteria.of("price", CriteriaOperator.GREATER_THAN_OR_EQUAL, priceFrom)
            );
        }

        if (priceTo != null) {
            criteriaList.add(
                    Criteria.of("price", CriteriaOperator.LESS_THAN_OR_EQUAL, priceTo)
            );
        }


        return productRepository.queryBuilder()
                .select(
                        Select("id", "id"),
                        Select("name", "name"),
                        Select("price", "price"),
                        Select("category"),
                        Select("productDetail")
                )
                .distinct(false)
                .where(criteriaList.toArray(new Criteria[0]))
                .orderBy(OrderBy("name", Order.DESC))
                .page(0, 3)
                .getResultAsPage(ProductDto.class);
    }

    public void remove(Integer id) {
        productRepository.deleteById(id);
    }

    @Transactional
    public Product save(ProductDto product) {
        CategoryEntity category = categoryRepository.findByName(product.getCategory().getName())
                .orElseThrow(() -> new RuntimeException("Category not found"));

        Product product1 = ProductMapper.toProduct(product);
        if (category != null) {
            product1.category = category;
        }
        return productRepository.save(product1);
    }
}
