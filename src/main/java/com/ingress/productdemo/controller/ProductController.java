package com.ingress.productdemo.controller;

import com.ingress.productdemo.model.Product;
import com.ingress.productdemo.model.dto.ProductDto;
import com.ingress.productdemo.service.ProductService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public Page<ProductDto> getProducts(@RequestParam(required = false) Integer priceFrom,
                                        @RequestParam(required = false) Integer priceTo) {
        return productService.findAllWithDynamicQuery(priceFrom, priceTo);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer id) {
        productService.remove(id);
       return ResponseEntity.ok().build();
    }


    @PostMapping
    public Product createProduct(@RequestBody ProductDto product) {
       return productService.save(product);
    }
}
