package com.ingress.productdemo.service;

import com.ingress.productdemo.model.ShoppingCart;
import com.ingress.productdemo.repository.ProductRepository;
import com.ingress.productdemo.repository.ShoppingCartRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartService {
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;

    public ShoppingCartService(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
    }

    public ShoppingCart getShoppingCart(Long id) {
        return shoppingCartRepository.findById(id).orElseThrow(() -> new RuntimeException("Shopping cart not found"));
    }

    public void addProductToCart(Long id, Long productId) {}

    public void createShoppingCart(String name) {

    }
}
