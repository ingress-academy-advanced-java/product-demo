package com.ingress.productdemo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Products")
@FieldDefaults(level = AccessLevel.PUBLIC)
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "ProductName", unique = true)
    String name;

    @Column(name = "ProductPrice", nullable = false)
    Integer price;

    @Column(name = "Test")
    String test;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "CategoryId")
    CategoryEntity category;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ProductDetailId")
    ProductDetail productDetail;

    @ManyToMany(mappedBy = "products", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private Set<ShoppingCart> students = new HashSet<>();



}
