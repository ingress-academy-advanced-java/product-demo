package com.ingress.productdemo.util;

import com.ingress.productdemo.model.CategoryEntity;
import com.ingress.productdemo.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseSeeder implements CommandLineRunner {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public void run(String... args) throws Exception {
        if (categoryRepository.count() == 0) {
            categoryRepository.save(CategoryEntity.builder()
                    .name("Kitab")
                    .build());
            categoryRepository.save(CategoryEntity.builder()
                    .name("Elektronika")
                    .build());
            categoryRepository.save(CategoryEntity.builder()
                    .name("Məişət")
                    .build());
        }
    }
}