package com.ingress.productdemo.helper;

import com.ingress.productdemo.model.Product;
import com.ingress.productdemo.model.dto.ProductDto;

public class ProductMapper {

    public static ProductDto toProductDto(Product product) {
        return ProductDto.builder()
                .name(product.name)
                .price(product.price)
                .category(product.category)
                .productDetail(product.productDetail)
                .build();
    }

    public static Product toProduct(ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .price(productDto.getPrice())
                .category(productDto.getCategory())
                .productDetail(productDto.getProductDetail())
                .build();
    }
}
